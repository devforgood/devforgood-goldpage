require('dotenv').config({ silent: true })
const express = require('express');
const goldpage = require('goldpage');
const PORT = process.env.PORT || 3000
const app = express();

const funql = require('funql-api')
funql.middleware(app, {
    /*defaults*/
    getMiddlewares: [],
    postMiddlewares: [],
    allowGet: false,
    allowOverwrite: false,
    attachToExpress: false,
    allowCORS: false,
    api: {
        async hello() {
            return `Hello`
        }
    }
})

app.use(goldpage.express);
app.listen(PORT, () => console.log(`LISTENING ON ${PORT}`))