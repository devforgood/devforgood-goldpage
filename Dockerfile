FROM node:13.5.0-alpine
WORKDIR /app
COPY . .
RUN npm i --production
CMD npm run prod